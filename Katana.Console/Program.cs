﻿using System;
using System.IO;

namespace Katana
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine();
            
            foreach (var drive in DriveInfo.GetDrives())
            {
                Console.WriteLine(drive.Name);
                Console.WriteLine(drive.VolumeLabel);
                Console.WriteLine(drive.DriveType);
                Console.WriteLine("------------------------------------------------------------------------------------------------------");
                
            }
        }
    }
}
