﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Katana.Web.Controllers
{
    [Route("api/file")]
    [ApiController]
    public class FileController : ControllerBase
    {

        [HttpGet]
        public ActionResult GetFile()
        {
            var stream = System.IO.File.Open(@"C:\Git\Katana\Katana.Console\Program.cs", FileMode.Open,
                FileAccess.ReadWrite);
            return File(stream, "application/octet-stream");
        } 
    }
}
